from task_script import write_data_from_url

URL_ENUM = [
    "https://www.tatneft.ru/",
    "https://www.tatneft.ru/karera/?lang=ru",
    "https://www.tatneft.ru/karera/obshchaya-informatsiya/?lang=ru",
]


if __name__ == "__main__":
    write_data_from_url(URL_ENUM)
