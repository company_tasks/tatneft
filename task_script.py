from datetime import datetime

import requests


def write_data_to_file(name: str, data: str):
    """Write data to file."""

    with open(f"files/{name}", "w") as f:
        f.write(data)


def write_data_from_url(urls: list):
    """Get data from url and write to file."""

    with requests.Session() as session:
        for url in urls:

            try:
                response = session.get(f"{url}")

                name = datetime.now().strftime("%Y_%m_%d_%H:%M:%S.%f")
                write_data_to_file(f"{name}.txt", response.text)

                print(f"Успешно загрузил {url}")

            except requests.RequestException:
                print(f"Не удалось загрузить {url}")
