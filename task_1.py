"""This module calculate simple numbers.

User must send args with start script.
For example: python task_1.py 1000000.
"""


import sys


def get_eratosthenes_enum(number: int) -> list:
    """Return list of simple numbers."""

    elements = list(range(number + 1))
    elements[1] = 0

    for i in elements:
        if i > 1:

            for j in range(i + i, len(elements), i):
                elements[j] = 0

    result = [x for x in elements if x != 0]
    return result


def get_input_data() -> int:
    """Return user input data."""

    args = sys.argv
    if len(args) < 2:
        print("Не передан параметр N!")
        return 0

    if args[1].isdigit():
        return int(args[1])

    else:
        print("Введите целое число больше 2!")
        return 0


if __name__ == "__main__":
    input_data = get_input_data()
    if input_data:

        print(get_eratosthenes_enum(input_data))
